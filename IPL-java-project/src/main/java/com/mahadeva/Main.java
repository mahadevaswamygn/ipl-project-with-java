package com.mahadeva;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Match> matchesData = readMatchesData();
        List<Delivery> deliveriesData = readDeliveriesData();

        findNumberOfMatchesPlayedPerYearOfTheAllYear(matchesData);
        findNumberOfMatchesWonByEachTeamOverAllYear(matchesData);
        findExtraRunsConcededPerTeamIn2016(matchesData, deliveriesData);
        findMostEconomicalBowlerIn2015(matchesData, deliveriesData);
        findEachBatsmanHitsHowManyNoOfBoundaryInEachVenueInAllYear(matchesData, deliveriesData);
        findBowlerWithMostFoursIn2017(matchesData, deliveriesData);
        findTheMaximumNumberOfWicketsTakenByBowlerIn2015ByVenue(matchesData, deliveriesData);
    }
    private static void findTheMaximumNumberOfWicketsTakenByBowlerIn2015ByVenue(List<Match> matchesData, List<Delivery> deliveriesData) {
        Map<String, Map<String, Integer>> maximumNumberOfWicketsIn2015ByVenue = new HashMap<>();
        List<String> matchesIdIn2015 = new ArrayList<>();
        for (Match match : matchesData) {
            if (match.getSeason().equals("2015")) {
                matchesIdIn2015.add(match.getId());
            }
        }
        Map<String, ArrayList<String>> venueAndThatMatchId = new HashMap<>();
        for (Match match : matchesData) {
            if (matchesIdIn2015.contains(match.getId())) {
                if (venueAndThatMatchId.containsKey(match.getVenue())) {
                    ArrayList<String> idsOfVenue = venueAndThatMatchId.get(match.getVenue());
                    String matchId = match.getId();
                    idsOfVenue.add(matchId);
                    venueAndThatMatchId.put(match.getVenue(), idsOfVenue);
                } else {
                    ArrayList<String> matchIds = new ArrayList<>();
                    matchIds.add(match.getId());
                    venueAndThatMatchId.put(match.getVenue(), matchIds);
                }
            }
        }
        for (Match match : matchesData) {
            if (!maximumNumberOfWicketsIn2015ByVenue.containsKey(match.getVenue())) {
                ArrayList<String> idsOfVenue = new ArrayList<>();
                for (Map.Entry<String, ArrayList<String>> venueAndIds : venueAndThatMatchId.entrySet()) {
                    if (venueAndIds.getKey().equals(match.getVenue())) {
                        for (String ids : venueAndIds.getValue()) {
                            idsOfVenue.add(ids);
                        }
                    }
                }
                Map<String, Integer> noOfWicketsByBowler = new HashMap<>();
                noOfWicketsByBowler=findTheWicketsTakenByBowler(idsOfVenue, deliveriesData);
                maximumNumberOfWicketsIn2015ByVenue.put(match.getVenue(), noOfWicketsByBowler);
            }
        }
        System.out.println();
        System.out.println("Highest wicket taker in 2015 each venue ");
        for (Map.Entry<String, Map<String, Integer>> bowlerStats : maximumNumberOfWicketsIn2015ByVenue.entrySet()) {
            Map<String, Integer> maximumWicketTakerNameAndHimsWickets = bowlerStats.getValue();
            for (Map.Entry<String, Integer> bowler : maximumWicketTakerNameAndHimsWickets.entrySet()) {
                if (bowler.getKey().equals("")) {
                    continue;
                }
                System.out.println(bowlerStats.getKey());
                System.out.println(bowler.getKey() + " = " + bowler.getValue());
            }
        }
    }
    private static Map<String, Integer> findTheWicketsTakenByBowler(ArrayList<String> idsOfVenue, List<Delivery> deliveriesData) {
        Map<String, Integer> wicketsByBowler = new HashMap<>();
        for (Delivery delivery : deliveriesData) {
            if (idsOfVenue.contains(delivery.getMatchId())) {
                if (wicketsByBowler.containsKey(delivery.getBowler())) {
                    int wickets = wicketsByBowler.get(delivery.getBowler());
                    if (delivery.getDismissalKind().equals("bowled") || delivery.getDismissalKind().equals("caught")) {
                        wickets++;
                    }
                    wicketsByBowler.put(delivery.getBowler(), wickets);
                } else {
                    if (delivery.getDismissalKind().equals("bowled") || delivery.getDismissalKind().equals("caught")) {
                        wicketsByBowler.put(delivery.getBowler(), 1);
                    }
                }
            }
        }
        String bowlerName = "";
        int highestWickets = 0;
        Map<String, Integer> highestWicketTaker = new HashMap<>();
        for (Map.Entry<String, Integer> wickets : wicketsByBowler.entrySet()) {
            if (highestWickets < wickets.getValue()) {
                bowlerName = wickets.getKey();
                highestWickets = wickets.getValue();
            }
        }
        highestWicketTaker.put(bowlerName, highestWickets);
        return highestWicketTaker;
    }
    private static List<Match> readMatchesData() {
        List<Match> matches = new ArrayList<>();
        String path = "/home/mahadeva/Downloads/updatep/ipl-project-with-java/IPL-java-project/src/main/resources/csvfiles/matches.csv";
        try {
            CSVParser parser = new CSVParser(new FileReader(path), CSVFormat.DEFAULT.withHeader());
            for (CSVRecord matchCsvRecord : parser) {
                Match match = new Match();
                match.setId(matchCsvRecord.get("id"));
                match.setSeason(matchCsvRecord.get("season"));
                match.setCity(matchCsvRecord.get("city"));
                match.setDate(matchCsvRecord.get("date"));
                match.setTeam1(matchCsvRecord.get("team1"));
                match.setTeam2(matchCsvRecord.get("team2"));
                match.setTossWinner(matchCsvRecord.get("toss_winner"));
                match.setTossDecision(matchCsvRecord.get("toss_decision"));
                match.setResult(matchCsvRecord.get("result"));
                match.setDlApplied(matchCsvRecord.get("dl_applied"));
                match.setWinner(matchCsvRecord.get("winner"));
                match.setWinByRuns(matchCsvRecord.get("win_by_runs"));
                match.setWinByWickets(matchCsvRecord.get("win_by_wickets"));
                match.setPlayerOfMatch(matchCsvRecord.get("player_of_match"));
                match.setVenue(matchCsvRecord.get("venue"));
                match.setUmpire1(matchCsvRecord.get("umpire1"));
                match.setUmpire2(matchCsvRecord.get("umpire2"));
                match.setUmpire3(matchCsvRecord.get("umpire3"));
                matches.add(match);
            }
            parser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matches;
    }
    private static List<Delivery> readDeliveriesData() {
        String path = "/home/mahadeva/Downloads/updatep/ipl-project-with-java/IPL-java-project/src/main/resources/csvfiles/deliveries.csv";
        List<Delivery> deliveries = new ArrayList<>();
        try {
            CSVParser parser = new CSVParser(new FileReader(path), CSVFormat.DEFAULT.withHeader());
            for (CSVRecord record : parser) {
                Delivery delivery = new Delivery();
                delivery.setMatchId(record.get("match_id"));
                delivery.setInnings(record.get("inning"));
                delivery.setBattingTeam(record.get("batting_team"));
                delivery.setBowlingTeam(record.get("bowling_team"));
                delivery.setOver(record.get("over"));
                delivery.setBall(record.get("ball"));
                delivery.setBatsman(record.get("batsman"));
                delivery.setNonStriker(record.get("non_striker"));
                delivery.setBowler(record.get("bowler"));
                delivery.setIsSuperOver(record.get("is_super_over"));
                delivery.setWideRuns(record.get("wide_runs"));
                delivery.setByeRuns(record.get("bye_runs"));
                delivery.setLegByeRuns(record.get("legbye_runs"));
                delivery.setNoBallRuns(record.get("noball_runs"));
                delivery.setPenaltyRuns(record.get("penalty_runs"));
                delivery.setBatsmanRuns(record.get("batsman_runs"));
                delivery.setExtraRuns(record.get("extra_runs"));
                delivery.setTotalRuns(record.get("total_runs"));
                delivery.setPlayerDismissed(record.get("player_dismissed"));
                delivery.setDismissalKind(record.get("dismissal_kind"));
                delivery.setFielder(record.get("fielder"));
                deliveries.add(delivery);
            }
            parser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deliveries;
    }
    private static void findNumberOfMatchesPlayedPerYearOfTheAllYear(List<Match> matches) {
        Map<String, Integer> noOfMatchesPlayedPerYearOfAllTheYear = new HashMap<>();
        for (Match match : matches) {
            if (noOfMatchesPlayedPerYearOfAllTheYear.containsKey(match.getSeason())) {
                int matchesPlayed = noOfMatchesPlayedPerYearOfAllTheYear.get(match.getSeason());
                matchesPlayed++;
                noOfMatchesPlayedPerYearOfAllTheYear.put(match.getSeason(), matchesPlayed);
            } else {
                noOfMatchesPlayedPerYearOfAllTheYear.put(match.getSeason(), 1);
            }
        }
        System.out.println("Number of matches played per year of all the year");
        for (Map.Entry<String, Integer> yearOfMatches : noOfMatchesPlayedPerYearOfAllTheYear.entrySet()) {
            System.out.println(yearOfMatches.getKey() + " : " + yearOfMatches.getValue());
        }
    }
    private static void findEachBatsmanHitsHowManyNoOfBoundaryInEachVenueInAllYear(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Map<String, Integer>> noOfBoundaryByBatsmanInEachVenue = new HashMap<>();
        for (Match match : matches) {
            if (!noOfBoundaryByBatsmanInEachVenue.containsKey(match.getVenue())) {
                Map<String, Integer> noOfBoundariesHitsByBatsman = new HashMap<>();
                noOfBoundariesHitsByBatsman=findNoOfBoundariesHitByBatsManInEachVenue(match.getVenue(), matches, deliveries);
                noOfBoundaryByBatsmanInEachVenue.put(match.getVenue(), noOfBoundariesHitsByBatsman);
            }
        }
        for (Map.Entry<String, Map<String, Integer>> venue : noOfBoundaryByBatsmanInEachVenue.entrySet()) {
            System.out.println("------------------------------------");
            System.out.println(venue.getKey());
            System.out.println("------------------------------------");
            Map<String, Integer> batsmanWithBoundaries = new HashMap<>();
            batsmanWithBoundaries= venue.getValue();
            for (Map.Entry<String, Integer> batsmanStats : batsmanWithBoundaries.entrySet()) {
                System.out.println(batsmanStats.getKey() + " " + batsmanStats.getValue());
            }
        }
    }
    private static Map<String, Integer> findNoOfBoundariesHitByBatsManInEachVenue(String venue, List<Match> matchesData, List<Delivery> deliveriesData) {
        Map<String, Integer> boundariesByBatsman = new HashMap<>();
        for (Match matches : matchesData) {
            if (matches.getVenue().equals(venue)) {
                for (Delivery delivery : deliveriesData) {
                    if (delivery.getMatchId().equals(matches.getId())) {
                        if (boundariesByBatsman.containsKey(delivery.getBatsman())) {
                            int boundary = boundariesByBatsman.get(delivery.getBatsman());
                            if (delivery.getBatsmanRuns().equals("4") || delivery.getBatsmanRuns().equals("6")) {
                                boundary++;
                            }
                            boundariesByBatsman.put(delivery.getBatsman(), boundary);
                        } else {
                            if (delivery.getBatsmanRuns().equals("4") || delivery.getBatsmanRuns().equals("6")) {
                                boundariesByBatsman.put(delivery.getBatsman(), 1);
                            } else {
                                boundariesByBatsman.put(delivery.getBatsman(), 0);
                            }
                        }
                    }
                }
            }
        }
        return boundariesByBatsman;
    }
    private static void findNumberOfMatchesWonByEachTeamOverAllYear(List<Match> matches) {
        Map<String, Integer> noOfMatchesWonAllTeamsOverAllTheYears = new HashMap<>();
        for (Match match : matches) {
            if (match.getWinner().equals("")) {
                continue;
            }
            if (noOfMatchesWonAllTeamsOverAllTheYears.containsKey(match.getWinner())) {
                int won = noOfMatchesWonAllTeamsOverAllTheYears.get(match.getWinner());
                won = won + 1;
                noOfMatchesWonAllTeamsOverAllTheYears.put(match.getWinner(), won);
            } else {
                noOfMatchesWonAllTeamsOverAllTheYears.put((match.getWinner()), 1);
            }
        }
        System.out.println();
        System.out.println("Number of matches won all teams over all the years");
        for (Map.Entry<String, Integer> noOfMatchesWonAllYears : noOfMatchesWonAllTeamsOverAllTheYears.entrySet()) {
            System.out.println(noOfMatchesWonAllYears.getKey() + " : " + noOfMatchesWonAllYears.getValue());
        }
    }
    private static List<Integer> findMatchesIdByYear(List<Match> matches, int year) {
        List<Integer> matchesIdByYears = new ArrayList<>();
        for (Match match : matches) {
            if (Integer.parseInt(match.getSeason()) == year) {
                matchesIdByYears.add(Integer.parseInt(match.getId()));
            }
        }
        return matchesIdByYears;
    }
    private static void findExtraRunsConcededPerTeamIn2016(List<Match> matches, List<Delivery> deliveryList) {
        List<Integer> matchesIdsIn2016 = findMatchesIdByYear(matches, 2016);
        Map<String, Integer> extraRunsConcededPerTeamIn2016 = new HashMap<>();
        for (Delivery delivery : deliveryList) {
            if (matchesIdsIn2016.contains(Integer.parseInt(delivery.getMatchId()))) {
                int extraRuns;
                if (extraRunsConcededPerTeamIn2016.containsKey(delivery.getBowlingTeam())) {
                    extraRuns = extraRunsConcededPerTeamIn2016.get(delivery.getBowlingTeam());
                    extraRuns = extraRuns + Integer.parseInt(delivery.getExtraRuns());
                } else {
                    extraRuns = Integer.parseInt(delivery.getExtraRuns());
                }
                extraRunsConcededPerTeamIn2016.put(delivery.getBowlingTeam(), extraRuns);
            }
        }
        System.out.println();
        System.out.println("Extra runs conceded per team in 2016");
        for (Map.Entry<String, Integer> extraRunsConcededPerTeam : extraRunsConcededPerTeamIn2016.entrySet()) {
            System.out.println(extraRunsConcededPerTeam.getKey() + " = " + extraRunsConcededPerTeam.getValue());
        }
    }
    private static void findMostEconomicalBowlerIn2015(List<Match> matches, List<Delivery> deliveryList) {
        List<Integer> matchIdesIn2015 = findMatchesIdByYear(matches, 2015);
        Map<String, Integer> noOfBallByBowler = new HashMap<>();
        for (Delivery delivery : deliveryList) {
            if (matchIdesIn2015.contains(Integer.parseInt(delivery.getMatchId()))) {
                if (noOfBallByBowler.containsKey(delivery.getBowler())) {
                    if (delivery.getNoBallRuns().equals("0") && delivery.getWideRuns().equals("0")) {
                        int ball = noOfBallByBowler.get(delivery.getBowler());
                        ball = ball + 1;
                        noOfBallByBowler.put(delivery.getBowler(), ball);
                    }
                } else {
                    if (delivery.getNoBallRuns().equals("0") && delivery.getWideRuns().equals("0")) {
                        noOfBallByBowler.put(delivery.getBowler(), 1);
                    }
                }
            }
        }
        Map<String, Double> noOfOversByBowler = new HashMap<>();
        for (Map.Entry<String, Integer> noOfOversOfTheBowler : noOfBallByBowler.entrySet()) {
            Double balls = (double) noOfOversOfTheBowler.getValue();
            noOfOversByBowler.put(noOfOversOfTheBowler.getKey(), balls / 6);
        }
        Map<String, Integer> bowlerGivenRuns = new HashMap<>();
        for (Delivery delivery : deliveryList) {
            if (matchIdesIn2015.contains(Integer.parseInt(delivery.getMatchId()))) {
                if (bowlerGivenRuns.containsKey(delivery.getBowler())) {
                    int runs = bowlerGivenRuns.get(delivery.getBowler());
                    runs = runs + Integer.parseInt(delivery.getTotalRuns());
                    bowlerGivenRuns.put(delivery.getBowler(), runs);
                } else {
                    bowlerGivenRuns.put(delivery.getBowler(), Integer.parseInt(delivery.getTotalRuns()));
                }
            }
        }
        Map<String, Double> bowlersEconomy = new HashMap<>();
        for (String bowlerName : bowlerGivenRuns.keySet()) {
            bowlersEconomy.put(bowlerName, bowlerGivenRuns.get(bowlerName) / noOfOversByBowler.get(bowlerName));
        }
        PriorityQueue<Double> minimumEconomyOfBowler = new PriorityQueue<>();
        for (Map.Entry<String, Double> bowlerEconomyValues : bowlersEconomy.entrySet()) {
            minimumEconomyOfBowler.add(bowlerEconomyValues.getValue());
        }
        Double lowerEconomy = minimumEconomyOfBowler.peek();
        System.out.println();
        System.out.println("most economy bowler in 2015");
        for (Map.Entry<String, Double> bowlerEconomy : bowlersEconomy.entrySet()) {
            if (bowlerEconomy.getValue() == lowerEconomy) {
                System.out.println(bowlerEconomy.getKey() + "    " + lowerEconomy);
            }
        }
    }
    private static void findBowlerWithMostFoursIn2017(List<Match> matches, List<Delivery> deliveryList) {
        List<Integer> matchIdsIn2017 = findMatchesIdByYear(matches, 2017);
        Map<String, Integer> whoGaveMoreFoursIn2017 = new HashMap<>();
        for (Delivery delivery : deliveryList) {
            if (matchIdsIn2017.contains(Integer.parseInt(delivery.getMatchId()))) {
                if (Integer.parseInt(delivery.getBatsmanRuns())== 4) {
                    if (whoGaveMoreFoursIn2017.containsKey(delivery.getBowler())) {
                        int noOfFours = whoGaveMoreFoursIn2017.get(delivery.getBowler());
                        whoGaveMoreFoursIn2017.put(delivery.getBowler(), noOfFours + 1);
                    } else {
                        if (Integer.parseInt(delivery.getBatsmanRuns()) == 4) {
                            whoGaveMoreFoursIn2017.put(delivery.getBowler(), 1);
                        }
                    }
                }
            }
        }
        int maxFours = 0;
        String bowlerName = "";
        for (Map.Entry<String, Integer> fours : whoGaveMoreFoursIn2017.entrySet()) {
            if (fours.getValue() > maxFours) {
                bowlerName = fours.getKey();
                maxFours = fours.getValue();
            }
        }
        System.out.println();
        System.out.println("More fours  given  by bowler in 2017 ");
        System.out.println(bowlerName + " : " + maxFours);
    }
}

