package com.mahadeva;

public class Delivery {
    private String matchId;
    private String innings;
    private String battingTeam;
    private String bowlingTeam;
    private String over;
    private String ball;
    private String batsman;
    private String nonStriker;
    private String bowler;
    private String isSuperOver;
    private String wideRuns;
    private String byeRuns;
    private String legByeRuns;
    private String noBallRuns;
    private String penaltyRuns;
    private String batsmanRuns;
    private String extraRuns;
    private String totalRuns;
    private String playerDismissed;
    private String dismissalKind;
    private String fielder;
    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }
    public void setInnings(String innings) {
        this.innings = innings;
    }
    public void setBattingTeam(String battingTeam) {
        this.battingTeam = battingTeam;
    }
    public void setBowlingTeam(String bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }
    public void setOver(String over) {
        this.over = over;
    }
    public void setBall(String ball) {
        this.ball = ball;
    }
    public void setBatsman(String batsman) {
        this.batsman = batsman;
    }
    public void setNonStriker(String nonStriker) {
        this.nonStriker = nonStriker;
    }
    public void setBowler(String bowler) {
        this.bowler = bowler;
    }
    public void setIsSuperOver(String isSuperOver) {
        this.isSuperOver = isSuperOver;
    }
    public void setWideRuns(String wideRuns) {
        this.wideRuns = wideRuns;
    }
    public void setByeRuns(String byeRuns) {
        this.byeRuns = byeRuns;
    }
    public void setLegByeRuns(String legByeRuns) {
        this.legByeRuns = legByeRuns;
    }
    public void setNoBallRuns(String noBallRuns) {
        this.noBallRuns = noBallRuns;
    }
    public void setPenaltyRuns(String penaltyRuns) {
        this.penaltyRuns = penaltyRuns;
    }
    public void setBatsmanRuns(String batsmanRuns) {
        this.batsmanRuns = batsmanRuns;
    }
    public void setExtraRuns(String extraRuns) {
        this.extraRuns = extraRuns;
    }
    public void setTotalRuns(String totalRuns) {
        this.totalRuns = totalRuns;
    }
    public void setPlayerDismissed(String playerDismissed) {
        this.playerDismissed = playerDismissed;
    }
    public void setDismissalKind(String dismissalKind) {
        this.dismissalKind = dismissalKind;
    }
    public void setFielder(String fielder) {
        this.fielder = fielder;
    }
    public String getMatchId() {
        return matchId;
    }
    public String getInnings() {
        return innings;
    }
    public String getBattingTeam() {
        return battingTeam;
    }
    public String getBowlingTeam() {
        return bowlingTeam;
    }
    public String getOver() {
        return over;
    }
    public String getBall() {
        return ball;
    }
    public String getBatsman() {
        return batsman;
    }
    public String getNonStriker() {
        return nonStriker;
    }
    public String getBowler() {
        return bowler;
    }
    public String getIsSuperOver() {
        return isSuperOver;
    }
    public String getWideRuns() {
        return wideRuns;
    }
    public String getByeRuns() {
        return byeRuns;
    }
    public String getLegByeRuns() {
        return legByeRuns;
    }
    public String getNoBallRuns() {
        return noBallRuns;
    }
    public String getPenaltyRuns() {
        return penaltyRuns;
    }
    public String getBatsmanRuns() {
        return batsmanRuns;
    }
    public String getExtraRuns() {
        return extraRuns;
    }
    public String getTotalRuns() {
        return totalRuns;
    }
    public String getPlayerDismissed() {
        return playerDismissed;
    }
    public String getDismissalKind() {
        return dismissalKind;
    }
    public String getFielder() {
        return fielder;
    }
}
